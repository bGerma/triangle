from decimal import Decimal
from math import radians, sin, asin, degrees, cos, sqrt, acos
from tabulate import tabulate


def solve_angle_using_sine(angle_b, side_b, side_a):
    """
    solves the angle_a using the sin rule
    ASS
    A = Sin-1(Sin(B)/b * a)
    :return angle_a:
    """
    angle_a = degrees(asin(sin(radians(angle_b)) / side_b * side_a))
    return angle_a


def solve_angle_using_cosine(side_a, side_b, side_c):
    """
    solves the angle_a
    SSS
    A = Cos-1((b^2 + c^2 - a^2) / 2bc)
    43.0 = Cos-1((3.6^2 + 9.4^2 - 7.2^2) / 2*3.6*9.4)
    :return angle_a:
    """
    side_a, side_b, side_c = map(Decimal, (side_a, side_b, side_c))
    angle_a = degrees(acos((side_b ** 2 + side_c ** 2 - side_a ** 2) / (2 * side_b * side_c)))
    return angle_a


def solve_side_using_sine(angle_a, angle_b, side_b):
    """
    AAS
    a = (b / Sin(B)) * Sin(A)
    :return side_a:
    """
    side_a = (Decimal(side_b) / Decimal(sin(angle_b))) * Decimal(sin(angle_a))
    return side_a


def solve_side_using_cosine(angle_a, side_b, side_c):
    """
    SAS
    a = √(b^2 + c^2 - 2bcCos(A))
    4.86 = √4.7^2 + 7.3^2 - 2*4.7*7.3*Cos(41)
    :return side_a:
    """
    side_b, side_c = map(Decimal, (side_b, side_c))
    side_a = sqrt((side_b ** 2) + (side_c ** 2) - (2 * side_b * side_c * Decimal(cos(radians(angle_a)))))
    return side_a


def solve_area_using_sine(side_a, angle_b, side_c):
    """
    solves the area
    SAS
    Area = 0.5 * a * b * Sin(C)
    :return Area of triangle:
    """
    area = side_a * side_c * sin(radians(angle_b)) * 0.5
    return area


def solve_area_using_heron_formula(side_a, side_b, side_c):
    """
    solves the area using heron's formula
    :param side_a: one side from a triangle
    :param side_b: one side from a triangle
    :param side_c: one side from a triangle
    :return: the area of the triangle
    """
    s = (side_a + side_b + side_c) / 2
    return sqrt(s * (s - side_a) * (s - side_b) * (s - side_c))


def get_side(angle_x=None, side_y=None, angle_y=None, side_z=None, angle_z=None, perimeter=None):
    if perimeter and side_y and side_z:
        return perimeter - (side_y + side_z)
    elif angle_x and side_y and side_z:
        return solve_side_using_cosine(angle_x, side_y, side_z)
    elif angle_x and angle_y and side_y:
        return solve_side_using_sine(angle_x, angle_y, side_y)
    elif angle_x and angle_z and side_z:
        return solve_side_using_sine(angle_x, angle_z, side_z)


def get_angle(side_x=None, side_y=None, angle_y=None, side_z=None, angle_z=None, total_degrees=None):
    if angle_y and angle_z:
        return total_degrees - (angle_y + angle_z)
    if side_x and side_y and side_z:
        return solve_angle_using_cosine(side_x, side_y, side_z)
    if side_y and angle_y and side_x:
        return solve_angle_using_sine(angle_y, side_y, side_x)
    if side_z and angle_z and side_x:
        return solve_angle_using_sine(angle_z, side_z, side_x)


class Triangle:
    """triangle"""

    def __init__(self, perimeter=None, area=None, height=None, width=None, angle_a=None, angle_b=None, angle_c=None,
                 side_a=None, side_b=None, side_c=None):
        """
        creates a triangle instance
        You must at least enter one of these:
        3 sides,
        2 sides and 1 angle,
        2 sides and perimeter
        """
        print(locals())
        self.total_degrees = 180
        self._can_calculate_sides = any((side_a, side_b, side_c))
        self._perimeter = perimeter
        self._area = area
        self._width = width
        self._height = height
        self._angle_a = angle_a
        self._angle_b = angle_b
        self._angle_c = angle_c
        self._side_a = side_a
        self._side_b = side_b
        self._side_c = side_c

        self._got_repeated_result = False
        self._last_loop_result = None
        while self._got_repeated_result is False:
            self.perimeter = self._get_perimeter()
            self.area = self._get_area()
            self.height = self._get_height()
            self.width = self._get_width()
            self.angle_a = self._get_angle_a()
            self.angle_b = self._get_angle_b()
            self.angle_c = self._get_angle_c()
            if self._can_calculate_sides is True:
                self.side_a = self._get_side_a()
                self.side_b = self._get_side_b()
                self.side_c = self._get_side_c()
            self._got_repeated_result = self._last_loop_result == self._get_unknowns()
            self._last_loop_result = self._get_unknowns()

    def __str__(self):
        return tabulate(tabular_data=[
            ['perimeter', self.perimeter],
            ['area', self.area],
            ['height', self.height],
            ['width', self.width],
            ['angle_a', self.angle_a],
            ['angle_b', self.angle_b],
            ['angle_c', self.angle_c],
            ['side_a', self.side_a],
            ['side_b', self.side_b],
            ['side_c', self.side_c]
        ], headers=['Property', 'Value'], tablefmt='fancy_grid')

    def _get_unknowns(self):
        try:
            return sum((
                property_ is None for property_ in [self.perimeter, self.area, self.height, self.width, self.angle_a,
                                                    self.angle_b, self.angle_c, self.side_a, self.side_b, self.side_c]
            ))
        except AttributeError:
            pass

    def _get_perimeter(self):
        """area of triangle"""
        if self._perimeter is not None:
            return self._perimeter
        elif all((self._side_a, self._side_b, self._side_c)):
            self._perimeter = sum((self._side_a, self._side_b, self._side_c))

        return self._perimeter

    def _get_area(self):
        """area of triangle"""
        if self._area is not None:
            return self._area
        if self._height and self._width:
            self._area = self._height * self._width
        elif all((self._side_a, self._side_b, self._side_c)):
            self._area = solve_area_using_heron_formula(self._side_a, self._side_b, self._side_c)

        return self._area

    def _get_height(self):
        """height of triangle"""
        if self._height is not None:
            return self._height
        elif self._width and self._area:
            self._height = self._area / self._width

        return self._height

    def _get_width(self):
        """width of triangle"""
        if self._width is not None:
            return self._width
        elif self._height and self._area:
            self._width = self._area / self._height
        elif any((self._side_a, self._side_b, self._side_c)):
            for side in (self._side_a, self._side_b, self._side_c):
                if side is not None:
                    self._width = side
                    break

        return self._width

    def _get_side_a(self):
        if self._side_a is not None:
            return self._side_a
        self._side_a = get_side(self._angle_a, self._side_b, self._angle_b,
                                self._side_c, self._angle_c, self._perimeter)

        return self._side_a

    def _get_angle_a(self):
        """gets the A angle of the triangle"""
        if self._angle_a is not None:
            return self._angle_a
        self._angle_a = get_angle(self._side_a, self._side_b, self._angle_b,
                                  self._side_c, self._angle_c, self.total_degrees)

        return self._angle_a

    def _get_side_b(self):
        if self._side_b is not None:
            return self._side_b
        self._side_b = get_side(self._angle_b, self._side_c, self._angle_c,
                                self._side_a, self._angle_a, self._perimeter)

        return self._side_b

    def _get_angle_b(self):
        if self._angle_b is not None:
            return self._angle_b
        self._angle_b = get_angle(self._side_b, self._side_c, self._angle_c,
                                  self._side_a, self._angle_a, self.total_degrees)

        return self._angle_b

    def _get_side_c(self):
        if self._side_c is not None:
            return self._side_c
        self._side_c = get_side(self._angle_c, self._side_a, self._angle_a,
                                self._side_b, self._angle_b, self._perimeter)

        return self._side_c

    def _get_angle_c(self):
        if self._angle_c is not None:
            return self._angle_c
        self._angle_c = get_angle(self._side_c, self._side_a, self._angle_a,
                                  self._side_b, self._angle_b, self.total_degrees)

        return self._angle_c
