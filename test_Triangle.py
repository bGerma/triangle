import pytest
import Triangle


@pytest.mark.parametrize('angle_a, side_a, side_b, expected_answer', [
    (50, 13, 15, 62.1),
    (60, 13, 15, 87.8),
    (64.2, 7, 3, 22.7),
    (64.2, 8, 5, 34.2),
    (59, 7.1, 4.3, 31.3),
    (52, 7.6, 4.7, 29.2),
    (115, 15.2, 6.9, 24.3),
    (120, 14.7, 6.4, 22.2),
])
def test_solve_angle_using_sine(angle_a, side_a, side_b, expected_answer):
    """tests angle_using_sin"""
    assert round(Triangle.solve_angle_using_sine(angle_a, side_a, side_b), 1) == expected_answer


@pytest.mark.parametrize('side_a, side_b, side_c, expected_answer', [
    (7.2, 9.4, 3.6, 43.0),
    (11.2, 8.9, 5.8, 97.0),
    (7.9, 7.9, 12.7, 36.5),
    (7.9, 7.9, 14.6, 22.5),
    (11.2, 8.9, 5.1, 102.9),
    (11.1, 7.1, 15.7, 38.8),
    (11.6, 7.8, 15.7, 45.1),
])
def test_solve_angle_using_cosine(side_a, side_b, side_c, expected_answer):
    """tests angle_using_sin"""
    assert round(Triangle.solve_angle_using_cosine(side_a, side_b, side_c), 1) == expected_answer


@pytest.mark.parametrize('angle_a, angle_b, side_b, expected_answer', [
    (55, 75, 13, 11),
    (50, 75, 14, 11.1),
    (31, 66, 7.1, 4),
    (31, 66, 8.2, 4.6),
    (54, 31, 6.1, 9.6),
    (59, 27, 6.8, 12.8),
    (23, 119, 7.8, 5.5),
    (21, 113, 8.6, 6.7),
])
def test_solve_side_using_sin(angle_a, angle_b, side_b, expected_answer):
    """tests angle_using_sin"""
    assert Triangle.solve_side_using_sine(angle_a, angle_b, side_b)


@pytest.mark.parametrize('angle_a, side_b, side_c, dp,  expected_answer', [
    (41, 4.7, 7.3, 2, 4.9),
    (32, 5.6, 8.6, 2, 4.9),
    (121, 8.6, 5.8, 1, 12.6),
    (117, 5.8, 8.6, 1, 12.4),
    (124, 17.1, 14.3, 1, 27.8),
    (119, 15.4, 17.1, 1, 28.0),
])
def test_solve_side_using_cosine(angle_a, side_b, side_c, dp, expected_answer):
    """tests angle_using_sin"""
    assert round(Triangle.solve_side_using_cosine(angle_a, side_b, side_c), 1) == expected_answer


@pytest.mark.parametrize('side_a, angle_b, side_c, expected_answer', [
    (10, 33, 12, 32.7),
    (8, 31, 10, 20.6),
    (8.8, 82, 5.7, 24.8),
    (7.3, 82, 5.7, 20.6),
    (5.6, 32, 5.6, 8.3),
    (5.5, 34, 5.5, 8.5),
    (9.2, 60, 9.2, 36.7),
    (5.2, 60, 5.2, 11.7),
    (7.7, 63, 6.3, 21.6),
    (7.7, 68, 7.3, 26.1),
])
def test_solve_area_using_sin(side_a, angle_b, side_c, expected_answer):
    """tests angle_using_sin"""
    assert round(Triangle.solve_area_using_sine(side_a, angle_b, side_c), 1) == expected_answer
